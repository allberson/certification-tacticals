/* AUTOMATICALLY GENERATE CODE */

using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpc.storm.binding.task.TaskPortType;

namespace br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType
{
	public interface BaseIEmptyTaskPortType : BaseITaskPortType, IEnvironmentKind 
	{
	}
}