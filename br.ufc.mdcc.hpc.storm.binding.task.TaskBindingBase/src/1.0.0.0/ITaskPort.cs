using br.ufc.mdcc.hpc.storm.binding.task.TaskPortType;

namespace br.ufc.mdcc.hpc.storm.binding.task.TaskBindingBase
{
    public interface ITaskPort<T> : BaseITaskPort<T>
		where T : ITaskPortType
	{
	}

}