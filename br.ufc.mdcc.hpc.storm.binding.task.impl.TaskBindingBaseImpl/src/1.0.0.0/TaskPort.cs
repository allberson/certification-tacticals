using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using br.ufc.mdcc.hpc.storm.binding.channel.Binding;
using br.ufc.mdcc.hpc.storm.binding.task.ActionType;
using br.ufc.mdcc.hpc.storm.binding.task.TaskBindingBase;
using br.ufc.mdcc.hpc.storm.binding.task.TaskPortType;
using br.ufc.pargo.hpe.kinds;

namespace br.ufc.mdcc.hpc.storm.binding.task.impl.TaskBindingBaseImpl
{

    public class LocalReceiveRequest : IReceiveRequest
    {
        public LocalReceiveRequest(object v)
        {
            this.v = v;
        }

        public void Cancel()
        {
            throw new NotImplementedException();
        }

        private object v;

        public object GetValue()
        {
            return v;
        }

        public void registerWaitingSet(AutoResetEvent waiting_set)
        {
            throw new NotImplementedException();
        }

        public CompletedStatus Test()
        {
            throw new NotImplementedException();
        }

        public void unregisterWaitingSet(AutoResetEvent waiting_set)
        {
            throw new NotImplementedException();
        }

        public CompletedStatus Wait()
        {
            return new LocalCompletedStatus();
        }

    }

    public class LocalCompletedStatus : CompletedStatus
    {
        public int? Count => -1;

        public Tuple<int, int> Source => null;

        public int Tag => -1;

        public bool Cancelled => false;

        int? Status.Count(Type type)
        {
            return null;
        }
    }


    public class TaskPort<T> : BaseTaskPort<T>, ITaskPort<T>
	  where T : ITaskPortType
	{

		public override void main()
		{
		}

		public override void after_initialize()
		{
			TraceFlag = true;
			Console.WriteLine("this.UnitSizeInFacet.Count=" + this.UnitSizeInFacet.Count);
			foreach (KeyValuePair<int, IDictionary<string, int>> rrr in this.UnitSizeInFacet)
				foreach (KeyValuePair<string, int> sss in rrr.Value)
					Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": : TASK PORT --- facet_instance=" + rrr.Key + " / unit_id=" + sss.Key + " / size=" + sss.Value + " --- " + this.CID.getInstanceName());
		}

		public new bool TraceFlag
		{
			get
			{
				return base.TraceFlag;
			}
			set
			{
				base.TraceFlag = value;
				Channel.TraceFlag = base.TraceFlag;
			}
		}

        private IDictionary<int, IDictionary<AutoResetEvent,object[]>> barrier = new Dictionary<int, IDictionary<AutoResetEvent, object[]>>();

        int count_barrier = 0;

		private RequestList local_synchronize_action(object[] action_id)
		{
			int tag = ActionTags.action_tags[(string)action_id[0]];

			RequestList request_list = new RequestList();

			int host_size = this.Hosts.Count - 1; // ignorando o componente system.

			// BARRIER AMONG USERS CONNECTED TO THE SAME TASK PORT OBJECT ... (when users are placed in the same virtual platform)
			AutoResetEvent my_barrier = null;
			Monitor.Enter(this);
			{
				count_barrier++;
				if (!barrier.ContainsKey(tag))
					barrier[tag] = new Dictionary<AutoResetEvent, object[]>();

                my_barrier = new AutoResetEvent(false);
                barrier[tag].Add(my_barrier, action_id);

                if (barrier[tag].Count < host_size)
				{
                    Console.WriteLine("local_synchronize_actions I (size={2})  --- WAIT IN BARRIER {0} * {1}", count_barrier, this.CID, host_size);
					Monitor.Exit(this);
					my_barrier.WaitOne();
					Monitor.Enter(this);
				}
				else
				{
                    Console.WriteLine("local_synchronize_actions I --- END BARRIER {0} * {1}", count_barrier, this.CID);
					foreach (AutoResetEvent e in barrier[tag].Keys)	e.Set();
                    my_barrier.WaitOne();
				}

				Console.WriteLine("local_synchronize_actions I --- MAKE REQUESTS * {0}", this.CID);

				foreach (KeyValuePair<AutoResetEvent, object[]> e in barrier[tag])
				{
					LocalReceiveRequest req = new LocalReceiveRequest(e.Value);
					request_list.Add(req);
				}
			}
			Monitor.Exit(this);

			Console.WriteLine("local_synchronize_actions II --- GO SECOND BARRIER * {0}", this.CID);

			Monitor.Enter(this);
			{
				count_barrier--;
				if (count_barrier > 0)
				{
					Console.WriteLine("local_synchronize_actions II --- BEGIN BARRIER {0} * {1}", count_barrier, this.CID);
					Monitor.Exit(this);
					my_barrier.WaitOne();
					Monitor.Enter(this);
				}
				else
				{
					Console.WriteLine("local_synchronize_actions II --- END BARRIER {0} * {1}", count_barrier, this.CID);
					foreach (AutoResetEvent e in barrier[tag].Keys)	e.Set();
                    my_barrier.WaitOne();
				}

				barrier[tag].Remove(my_barrier);
				if (barrier[tag].Count == 0)
					barrier.Remove(tag);
                
                Console.WriteLine("local_synchronize_actions II --- FINISH {0} / {1} / {2} / {3}", count_barrier, barrier.ContainsKey(tag) ? barrier[tag].Count : -1, barrier.Count, this.CID);
			}
			Monitor.Exit(this);

			Console.WriteLine("local_synchronize_actions --- END * {0}", this.CID);
			return request_list;
		}

		private RequestList synchronize_action(object[] action_id)
		{
			foreach (object a in action_id)  Console.WriteLine("action is {0} {1}", a, this.TraceFlag);

            RequestList request_list = local_synchronize_action(action_id);

			foreach (string k in ActionTags.action_tags.Keys)
				Console.WriteLine("ActionTags.action_tags[{0}]={1}", k, ActionTags.action_tags[k]);

			//string[] value = (string[])action_id;
			int tag = ActionTags.action_tags[(string)action_id[0]];			

            foreach (object a in action_id) Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": synchronize_action={0} tag={1} --- {2}", a, tag, Channel.UnitSizeInFacet.Count);

            foreach (KeyValuePair<int, IDictionary<string, int>> facet in Channel.UnitSizeInFacet)
                if (facet.Key != this.ThisFacetInstance)
                {
                    Console.WriteLine("synchronize_action -- YES SEND REQUEST {0}={1}", facet.Key, this.ThisFacetInstance);
                    foreach (KeyValuePair<string, int> unit_team in facet.Value)
                        for (int i = 0; i < unit_team.Value; i++)
                        {
                            Console.WriteLine(this.ThisFacetInstance + "/" + ": synchronize_action " + action_id + " LOOP SEND " + facet.Key + "/" + i);
                            IRequest req = Channel.ImmediateSend<object[]>(action_id, new Tuple<int, int>(facet.Key, i), tag);
                            request_list.Add(req);
                        }
                }
                else
                    Console.WriteLine("synchronize_action -- NO SEND REQUEST {0}={1}", facet.Key, this.ThisFacetInstance);

            foreach (object a in action_id)  Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": synchronize_action " + a + " 1");

            foreach (KeyValuePair<int, IDictionary<string, int>> facet in Channel.UnitSizeInFacet)
                if (facet.Key != this.ThisFacetInstance)
                {
					Console.WriteLine("synchronize_action -- YES RECV REQUEST {0}={1}", facet.Key, this.ThisFacetInstance);
					foreach (KeyValuePair<string, int> unit_team in facet.Value)
                        for (int i = 0; i < unit_team.Value; i++)
                        {
                            foreach (object a in action_id) Console.WriteLine(this.ThisFacetInstance + "/" + ": synchronize_action " + a + " LOOP RECV 1 " + facet.Key + "/" + i);
                            IReceiveRequest req = Channel.ImmediateReceive<object[]>(new Tuple<int, int>(facet.Key, i), tag);
                            foreach (object a in action_id) Console.WriteLine(this.ThisFacetInstance + "/" + ": synchronize_action " + a + " LOOP RECV 2 " + facet.Key + "/" + i);
                            request_list.Add(req);
                        }
                }
				else
					Console.WriteLine("synchronize_action -- NO RECV REQUEST {0}={1}", facet.Key, this.ThisFacetInstance);

			foreach (object a in action_id)  Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": synchronize_action " + a + " 2");

            return request_list;
		}

		#region ITaskPort implementation

		public override void invoke(object action_id)
		{
			object invoke_lock;
			if (!action_lock.TryGetValue(action_id, out invoke_lock))
			{
				invoke_lock = new object();
				action_lock[action_id] = invoke_lock;
			}

			lock (invoke_lock)
			{
				Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE SYNC " + action_id + " BEFORE LOCK");
                RequestList request_list = synchronize_action(new object[] { action_id });

				Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE SYNC " + action_id + " BEFORE WAIT ALL");
				request_list.WaitAll();
                Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE SYNC " + action_id + " AFTER WAIT ALL");
			}
		}

		public override object invoke(object[] action_id)
		{
            object result;
            int fail = 0;
			object invoke_lock;
			if (!action_lock.TryGetValue(action_id, out invoke_lock))
			{
				invoke_lock = new object();
				action_lock[action_id] = invoke_lock;
			}

			lock (invoke_lock)
			{
				Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " BEFORE LOCK");
				RequestList request_list = synchronize_action(action_id);

				Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " BEFORE WAIT ALL");
				request_list.WaitAll();
				Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " AFTER WAIT ALL 1");

                result = action_id.Length == 1 ? action_id[0] : null;
				Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " AFTER WAIT ALL 2");
				foreach (IRequest req in request_list.Requests)
				{
					Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " AFTER WAIT ALL 3");
					if (req is IReceiveRequest)
                    {
                        Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " TYPE OF REQUEST is {0} --- {1}", req.GetType().ToString(), result);
                        IReceiveRequest recv_req = (IReceiveRequest)req;
                        Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " TYPE OF REQUEST is {0} --- {1}", recv_req.GetValue().GetType().ToString(), result);
                        object[] values = (object[])recv_req.GetValue();
                        if (values.Length == 1 && result == null)
                            result = values[0];
                        else if (values.Length == 1 && !result.Equals(values[0]))
                            fail++;
                    }
				}

				Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE [] SYNC " + action_id + " END INVOKE");
			}
            if (fail > 0)
            {
                foreach (object a in action_id)
                    Console.WriteLine("{0}: ACTION ACTIVATION INCONSISTENCY: action_id = {1}, fail = {2}", this.ThisFacetInstance, a, fail);
                throw new Exception("ACTION ACTIVATION INCONSISTENCY");
            }
            return result;
		}

		private IDictionary<object, object> action_lock = new Dictionary<object, object>();


		public override void invoke(object action_id, out IActionFuture future)
		{
			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 0");

			RequestList request_list = synchronize_action(new object[] { action_id });

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 1");

			ManualResetEvent sync = new ManualResetEvent(false);

			ActionFuture future_ = new ActionFuture(request_list, sync);
			future = future_;

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 2");

            Thread t = new Thread(new ThreadStart(() => handle_request(new object[] { action_id }, future_, sync)));

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 3");

			t.Start();

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 4");
		}

		public override void invoke(object[] action_id, out IActionFuture future)
		{
			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 0");

			RequestList request_list = synchronize_action(action_id);

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 1");

			ManualResetEvent sync = new ManualResetEvent(false);

			ActionFuture future_ = new ActionFuture(request_list, sync);
			future = future_;

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 2");

			Thread t = new Thread(new ThreadStart(() => handle_request(action_id, future_, sync)));

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 3");

			t.Start();

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE FUTURE " + action_id + " 4");
		}

        object handle_request(object[] action_id, ActionFuture future, ManualResetEvent sync)
        {
            object result;
            int fail = 0;

            Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": HANDLE REQUEST 1");
            future.RequestList.WaitAll();
            Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": HANDLE REQUEST 2");

			result = action_id.Length == 1 ? action_id[0] : null;
			foreach (IRequest req in future.RequestList.Requests)
			{
				if (req is ReceiveRequest)
				{
					IReceiveRequest recv_req = (IReceiveRequest)req;
					object[] values = (object[])recv_req.GetValue();
                    Console.WriteLine("{0}: HANDLE_REQUEST -- {1} -- {2}", this.ThisFacetInstance, values == null, result==null ? "NULL" : result);
					if (values.Length == 1 && result == null)
						result = values[0];
					else if (values.Length == 1 && !result.Equals(values[0]))
						fail++;
				}
			}

			if (fail > 0)
			{
				foreach (object a in action_id)
					Console.WriteLine("{0}: ACTION ACTIVATION INCONSISTENCY: action_id = {1}, fail = {2}", this.ThisFacetInstance, a, fail);
				throw new Exception("ACTION ACTIVATION INCONSISTENCY");
			}

			future.Action = result;

			sync.Set();
            Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": HANDLE REQUEST 3");
            future.setCompleted();
            Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": HANDLE REQUEST 4");

			return result;
		}

		void handle_request(object[] action_id, ActionFuture future, ManualResetEvent sync, Action[] reaction)
		{
			object result = handle_request(action_id, future, sync);

            for (int i = 0; i < action_id.Length;i++)
                if (action_id[i].Equals(result))
                    reaction[i]();
		}

		public override void invoke(object action_id, Action reaction, out IActionFuture future)
		{
			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 0");

			RequestList request_list = synchronize_action(new object[] { action_id });

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 1");

			ManualResetEvent sync = new ManualResetEvent(false);

			ActionFuture future_ = new ActionFuture(request_list, sync);
			future = future_;

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 2");

            Thread t = new Thread(new ThreadStart(() => handle_request(new object[] { action_id }, future_, sync, new Action[] { reaction })));

			t.Start();

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 3");

			//return t;
		}

		public override void invoke(object[] action_id, Action[] reaction, out IActionFuture future)
		{
			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 0");

			RequestList request_list = synchronize_action(new object[] { action_id });

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 1");

			ManualResetEvent sync = new ManualResetEvent(false);

			ActionFuture future_ = new ActionFuture(request_list, sync);
			future = future_;

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 2");

			Thread t = new Thread(new ThreadStart(() => handle_request(action_id, future_, sync, reaction)));

			t.Start();

			Console.WriteLine(this.ThisFacetInstance + "/" + /* this.Rank  + */": INVOKE ACTION " + action_id + " 3");

			//return t;
		}

		#endregion
	}

	internal class ActionFuture : IActionFuture
	{
		private RequestList request_list = null;
		private ManualResetEvent sync = null;
		private bool completed = false;

		public ActionFuture(RequestList request_list)
		{
			this.request_list = request_list;
		}

		public ActionFuture(RequestList request_list, ManualResetEvent sync)
		{
			this.request_list = request_list;
			this.sync = sync;
		}

		private object action;

		public object Action
		{
			get { return action; }
            set { action = value; }
		}

		#region ActionFuture implementation
		public void wait()
		{
			if (!completed)
				sync.WaitOne();
		}
		public bool test()
		{
			return completed;
		}

		public IActionFutureSet createSet()
		{
			IActionFutureSet afs = new ActionFutureSet();
			afs.addAction(this);
			return afs;
		}

		#endregion

		public object waiting_lock = new object();

		public void setCompleted()
		{
			lock (waiting_lock)
			{
				completed = true;
				foreach (AutoResetEvent waiting_set in waiting_sets)
					waiting_set.Set();
			}
		}

		private IList<AutoResetEvent> waiting_sets = new List<AutoResetEvent>();

		public RequestList RequestList { get { return request_list; } }

		public void registerWaitingSet(AutoResetEvent waiting_set)
		{
			lock (waiting_lock)
			{
				if (completed)
					waiting_set.Set();
				waiting_sets.Add(waiting_set);
			}
		}

		public void unregisterWaitingSet(AutoResetEvent waiting_set)
		{
			waiting_sets.Remove(waiting_set);
		}
	}

        internal class ActionFutureSet : IActionFutureSet
        {
            IList<IActionFuture> pending_list = new List<IActionFuture>();
            IList<IActionFuture> future_list = new List<IActionFuture>();

            #region ActionFutureSet implementation
            public void addAction (IActionFuture new_future)
            {
                lock (sync_oper)
                {
                    pending_list.Add(new_future);
                    future_list.Add(new_future);
                }

                if (sync_future != null)
                    new_future.registerWaitingSet (sync_future);
            }

            public void waitAll ()
            {
                foreach (IActionFuture action_future in pending_list) 
                    lock(sync_oper) action_future.wait ();          

                pending_list.Clear ();
            }

            AutoResetEvent sync_future = null;

            public IActionFuture waitAny ()
            {
                sync_future = new AutoResetEvent(false);

                lock (sync_oper)
                    foreach (IActionFuture action_future in pending_list) 
                        action_future.registerWaitingSet (sync_future); 

                sync_future.WaitOne ();
                sync_future = null;

                IActionFuture f = this.testAny ();

                lock (sync_oper)
                    foreach (IActionFuture action_future in pending_list) 
                        action_future.unregisterWaitingSet (sync_future);   


                /*  while (true)
            {
                Thread.Sleep (200);
                f = testAny();
                if (f != null)
                    return f;
            }*/


                return f;
            } 

            private object sync_oper = new object (); 

            public bool testAll ()
            {
                lock (sync_oper) 
                {
                    bool completed = true;
                    IList<IActionFuture> tobeRemoved = new List<IActionFuture> ();

                    foreach (IActionFuture action_future in pending_list) 
                    {
                        bool one_completed = action_future.test ();
                        if (one_completed)
                            tobeRemoved.Add (action_future);
                        completed = completed && one_completed;
                    }

                    foreach (IActionFuture f in tobeRemoved) 
                        pending_list.Remove (f);

                    return completed;
                }
            }

            public IActionFuture testAny ()
            {
                IActionFuture completed_action_future = null;

                lock (sync_oper) 
                {               
                    foreach (IActionFuture action_future in pending_list) 
                    {
                        if (action_future.test ()) 
                        {
                            completed_action_future = action_future;
                            break;
                        }
                    }
                    pending_list.Remove (completed_action_future);
                }

                return completed_action_future;
            }

			public IEnumerator<IActionFuture> GetEnumerator()
			{
				return future_list.GetEnumerator();
			}

			IEnumerator IEnumerable.GetEnumerator()
			{
				return future_list.GetEnumerator();
			}


		    public IActionFuture[] Pending 
            {
                get 
                {
                    lock (sync_oper) 
                    {               
                        IActionFuture[] f = new IActionFuture[pending_list.Count];
                        pending_list.CopyTo (f, 0);
                        return f;
                    }
                }
            }
            #endregion
        }
}
